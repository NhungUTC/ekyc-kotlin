package com.hyperlogy.smartekyc.ekyc.EkycModel.OCR

import java.io.Serializable

class UserInfor {

    data class IdCard(
        val identCardType : String,
        val identCardNumber : String,
        val identCardName : String,
        val identCardNameOther : String,
        val identCardBirthDate : String,
        val identCardGender : String,
        val identCardNation : String,
        val identCardEthnic : String,
        val identCardCountry : String,
        val identCardAdrResidence : String,
        val identCardExpireDate : String,
        val identCardIssueDate : String,
        val identCardIssuePlace : String,
        val identCardRaw : String,
        val errorDataFront : String,
        val outputDataFront : String,
        val msgDataFront : String,
        val errorDataBack : String,
        val outputDataBack : String,
        val msgDataBack : String
    ):Serializable {
        override fun toString(): String {
            return "IdCard(identCardType='$identCardType', identCardNumber=$identCardNumber, identCardName='$identCardName', identCardNameOther='$identCardNameOther', identCardBirthDate='$identCardBirthDate', identCardGender='$identCardGender', identCardNation='$identCardNation', identCardEthnic='$identCardEthnic', identCardCountry='$identCardCountry', identCardAdrResidence='$identCardAdrResidence', identCardExpireDate='$identCardExpireDate', identCardIssueDate='$identCardIssueDate', identCardIssuePlace='$identCardIssuePlace', identCardRaw='$identCardRaw', errorDataFront='$errorDataFront', outputDataFront='$outputDataFront', msgDataFront='$msgDataFront', errorDataBack='$errorDataBack', outputDataBack='$outputDataBack', msgDataBack='$msgDataBack')"
        }
    }
     data class DataOCR (val idCard : IdCard,
                    val rsCus : String,
                    val guidID : String,
                    val urlXml : String,
                    val cif : String
     ):Serializable{
        override fun toString(): String {
            return "DataOCR(idCard=$idCard, rsCus='$rsCus', guidID='$guidID', urlXml='$urlXml', cif='$cif')"
        }
    }
    data class RootOCR (
        val messages : String,
        val code : Int,
        val success : Boolean,
        val data : DataOCR
    ) : Serializable {
        override fun toString(): String {
            return "RootOCR(messages='$messages', code=$code, success=$success, data=$data)"
        }
    }
}