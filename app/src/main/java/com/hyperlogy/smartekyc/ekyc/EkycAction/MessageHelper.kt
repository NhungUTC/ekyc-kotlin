package com.hyperlogy.smartekyc.ekyc.EkycAction

import android.app.AlertDialog
import android.content.Context
import com.hyperlogy.smartekyc.ekyc.EkycAction.EkycMyCall.OnMyBtnCancleDialog
import com.hyperlogy.smartekyc.ekyc.EkycAction.EkycMyCall.OnMyBtnDialog

class MessageHelper {
    companion object {
        var messCode500: String="Có lỗi xảy ra trong quá trình xác thực chứng minh. Vui lòng thử lại"
        var messCode500Liveness = "Lỗi hệ thống. Vui lòng thử lại"
        var messageCheckInternet = "Mời bạn kiểm tra kết nối mạng và thử lại"
        var messageTimeout = "Quá thời gian phản hồi và mời bạn thử lại"
        var messageResponse = "Yêu cầu thất bại, mời bạn gửi lại"
        fun openDialog(context: Context,title: String, message: String, btnBoQua :String, btnThuLai:String,myBtnClick: OnMyBtnDialog ) {
        val b= AlertDialog.Builder(context)
            b.setTitle(title)
            b.setMessage(message)
             b.setPositiveButton(btnBoQua){
                 dialog, which ->
                 myBtnClick.onCancle(true)
             }
            b.setNegativeButton(btnThuLai){
                dialog, which ->
                myBtnClick.onTryAgain(true)
            }
            b.show()
        }
        fun openDialogCancle(context: Context, title: String, message: String, btnBoQua: String, myBtnCancleCLick: OnMyBtnCancleDialog){
            val  b= AlertDialog.Builder(context)
            b.setTitle(title)
            b.setMessage(message)
            b.setPositiveButton(btnBoQua){
                dialog, which ->
                myBtnCancleCLick.onCancle(true)
            }
            b.show()
        }
    }
}