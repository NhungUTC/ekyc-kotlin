package com.hyperlogy.smartekyc.ekyc.EkycAction.EkycMyCall

interface OnMyBtnDialog {
    fun onCancle(isCancle: Boolean)
    fun onTryAgain(isTryAgain :Boolean)
}