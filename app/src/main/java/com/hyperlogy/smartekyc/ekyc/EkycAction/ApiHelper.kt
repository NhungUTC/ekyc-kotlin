package com.hyperlogy.smartekyc.ekyc.EkycAction

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.hyperlogy.smartekyc.ekyc.EkycAction.EkycMyCall.OnMyBtnCancleDialog
import com.hyperlogy.smartekyc.ekyc.EkycAction.EkycMyCall.OnMyBtnDialog
import com.hyperlogy.smartekyc.ekyc.EkycAction.EkycMyCall.OnPostDataSuccess
import com.hyperlogy.smartekyc.ekyc.EkycActivity.LoginActivity
import com.hyperlogy.smartekyc.ekyc.EkycUntil.EkycCheckInternet.CheckInternet
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import org.json.JSONObject
import java.io.IOException
import java.util.concurrent.TimeUnit

class ApiHelper {
    companion object {
        var MEDIA_TYPE = "image/jpeg".toMediaType()
        var TIME_OUT_GET: Long=60
        var TIME_OUT_POST: Long=60
        fun OkhttpGet(
            context: Context,
            urlGet: String,
            myCallBack: OnPostDataSuccess
        ){
            if(CheckInternet.haveNetworkConnection(context)){
                val dialog: ProgressDialog
                dialog = ProgressDialog(context)
                dialog.setMessage("Đang thực hiện...")
                dialog.show()
                val okhttpCient = OkHttpClient.Builder()
                    .connectTimeout(TIME_OUT_GET, TimeUnit.SECONDS) // connect timeout
//                    .writeTimeout(5, TimeUnit.MINUTES) // write timeout
//                    .readTimeout(5, TimeUnit.MINUTES) // read timeout
                    .build()
                val request = Request.Builder()
                    .url(urlGet)
                    .get()
                    .build()
                okhttpCient.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        println("error get token")
                        dialog.cancel()
                        //timeout
                        Handler(Looper.getMainLooper()).post(){
                            //  CheckInternet.dialogGetToken(context,"Thông báo","Timeout","Làm lại")
                            MessageHelper.openDialog(context,"Lỗi", MessageHelper.messageTimeout,"Bỏ qua", "Thử lại",object: OnMyBtnDialog{
                                override fun onCancle(isCancle: Boolean) {
                                    if (isCancle) {
                                        val intent: Intent= Intent(context, LoginActivity::class.java)
                                        context.startActivity(intent)
                                    }
                                }

                                override fun onTryAgain(isTryAgain: Boolean) {
                                       if (isTryAgain) OkhttpGet(context,urlGet,myCallBack)
                                }

                            });
                        }
                    }

                    override fun onResponse(call: Call, response: Response) {
                        Handler(Looper.getMainLooper()).post(){
                            dialog.cancel()
                            if(response.code==200){
                                var body = response.body?.string()
                                Log.d("resGetToken", body + "")
                                var obj = JSONObject(body)
                                var token: String = obj.getString("Token")
                                myCallBack.onPostSuccess(body.toString())
                            }else{
                                MessageHelper.openDialogCancle(context,"Thông báo", MessageHelper.messageResponse,"Đăng nhập lại", object: OnMyBtnCancleDialog{
                                    override fun onCancle(isCancle: Boolean) {
                                        val intent: Intent= Intent(context, LoginActivity::class.java)
                                        context.startActivity(intent)
                                    }

                                });
                            }
                        }

                    }

                })
            }
            else{
              //  CheckInternet.dialogGetToken(context,"Thông báo","Bạn hãy kiểm tra lại kết nối Internet","OK")
                //check internet
                Handler(Looper.getMainLooper()).post(){
                    MessageHelper.openDialog(context,"Thông báo", MessageHelper.messageCheckInternet,"Bỏ qua", "Thử lại",object: OnMyBtnDialog{
                        override fun onCancle(isCancle: Boolean) {
                            if (isCancle) {
                                val intent: Intent= Intent(context, LoginActivity::class.java)
                                context.startActivity(intent)
                            }
                        }

                        override fun onTryAgain(isTryAgain: Boolean) {
                            if (isTryAgain) OkhttpGet(context,urlGet,myCallBack)
                        }

                    });
                }
            }
        }


        fun OkHttpPost(
            context: Context,
            urlPost: String,
            requestBody: RequestBody,
            isToken: Boolean,
            myCallBack: OnPostDataSuccess
        ) {
            if(CheckInternet.haveNetworkConnection(context)){
                //okhttp
                val client = OkHttpClient.Builder()
                    .connectTimeout(TIME_OUT_POST, TimeUnit.SECONDS) // connect timeout
//                    .writeTimeout(5, TimeUnit.MINUTES) // write timeout
//                    .readTimeout(5, TimeUnit.MINUTES) // read timeout
                    .build()
                var request: Request? = null
                if (isToken == true) {
                    val token: String = GetData.getToken(context)
                    request = Request.Builder()
                        .url(urlPost)
                        .post(requestBody!!)
                        .header("Authorization", "Bearer " + token)
                        .addHeader("content-type", "application/json; charset=utf-8")
                        .build()
                }else{
                     request = Request.Builder()
                        .url(urlPost)
                        .post(requestBody)
                        .addHeader("content-type", "multipart/form-data")
                        .build()
                }

                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        Handler(Looper.getMainLooper()).post(){
                            Log.d("timeout", e.toString())
                           // CheckInternet.checkTimeoutPost(context,"Thông báo","Timeout","Đăng nhập lại")
                            //timeout
                            MessageHelper.openDialog(context,"Thông báo", MessageHelper.messageTimeout,"Bỏ qua", "Thử lại",object: OnMyBtnDialog{
                                override fun onCancle(isCancle: Boolean) {
                                    if (isCancle) {
                                        val intent: Intent= Intent(context, LoginActivity::class.java)
                                        context.startActivity(intent)
                                    }
                                }

                                override fun onTryAgain(isTryAgain: Boolean) {
                                    if (isTryAgain) OkHttpPost(context,urlPost, requestBody, isToken,myCallBack)
                                }

                            });
                        }
                    }

                    override fun onResponse(call: Call, response: Response) {
                            var res = response.toString()
                            var codeCheck: Int= response.code
                            if(codeCheck==200){
                                Log.d("res_post_img1", res)
                                var body = response.body?.string()
                                Log.d("res_post_img", body.toString())
                                myCallBack.onPostSuccess(body!!)
                            }else{
                               // CheckInternet.checkTimeoutPost(context,"Thông báo","Yêu cầu thất bại, mời bạn gửi lại","Đang nhập lại")
                                //code khac 200
                                MessageHelper.openDialogCancle(context,"Thông báo",MessageHelper.messageResponse,"Đăng nhập lại", object : OnMyBtnCancleDialog{
                                    override fun onCancle(isCancle: Boolean) {
                                        val intent: Intent= Intent(context, LoginActivity::class.java)
                                        context.startActivity(intent)
                                    }

                                })

                            }
                        }
                })
            }else{
               Handler(Looper.getMainLooper()).post(){
                   MessageHelper.openDialog(context,"Lỗi", MessageHelper.messageTimeout,"Bỏ qua", "Thử lại",object: OnMyBtnDialog{
                       override fun onCancle(isCancle: Boolean) {
                           if (isCancle) {
                               val intent: Intent= Intent(context, LoginActivity::class.java)
                               context.startActivity(intent)
                           }
                       }

                       override fun onTryAgain(isTryAgain: Boolean) {
                           if (isTryAgain) OkHttpPost(context,urlPost, requestBody, isToken,myCallBack)
                       }

                   });
               }

            }

        }

    }


}
