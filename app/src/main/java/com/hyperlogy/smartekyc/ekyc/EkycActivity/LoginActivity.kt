package com.hyperlogy.smartekyc.ekyc.EkycActivity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.hyperlogy.smartekyc.ekyc.EkycAction.ApiHelper
import com.hyperlogy.smartekyc.ekyc.EkycAction.GetData
import com.hyperlogy.smartekyc.ekyc.EkycAction.EkycMyCall.OnPostDataSuccess
import com.hyperlogy.smartekyc.ekyc.R
import com.hyperlogy.smartekyc.ekyc.EkycUntil.EkycApi.Api
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnGetTokenClick();
        //  setCompany()
    }


    private fun btnGetTokenClick() {
        btn_get_token.setOnClickListener {
            getToken();
        }
        btn_get_token.performClick()
    }
    private fun getToken() {
        val urlGetToken: String = Api.getUrlGetToken().toString()
        ApiHelper.OkhttpGet(this@LoginActivity, urlGetToken, object : OnPostDataSuccess {
            override fun onPostSuccess(response: String) {
                super.onPostSuccess(response)
                var obj = JSONObject(response)
                var token: String = obj.getString("Token")
                Log.d("getToken", token)
                //sharedPreference
                GetData.saveToken(this@LoginActivity, token)
                val intent: Intent = Intent(this@LoginActivity, OcrActivity::class.java)
                startActivity(intent)
            }
        })

    }
}