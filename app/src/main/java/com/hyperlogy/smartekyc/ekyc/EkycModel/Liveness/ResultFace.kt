package com.hyperlogy.smartekyc.ekyc.EkycModel.Liveness

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ResultFace {
    data class DataLiveness (
        @SerializedName("livenessResult") val livenessResult : Boolean,
        @SerializedName("matchingResult") val matchingResult : Boolean,
        @SerializedName("matchingRate") val matchingRate : Double,
        @SerializedName("imageNumber") val imageNumber : Int
    ):Serializable{
        override fun toString(): String {
            return "Data(livenessResult=$livenessResult, matchingResult=$matchingResult, matchingRate=$matchingRate, imageNumber=$imageNumber)"
        }
    }
    data class RootLiveness (
        @SerializedName("messages") val messages : String,
        @SerializedName("code") val code : Int,
        @SerializedName("success") val success : Boolean,
        @SerializedName("data") val data : DataLiveness
    ):Serializable{
        override fun toString(): String {
            return "RootLiveness(messages='$messages', code=$code, success=$success, data=$data)"
        }
    }
}