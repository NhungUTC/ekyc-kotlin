package com.hyperlogy.smartekyc.ekyc.EkycActivity

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.hyperlogy.smartekyc.ekyc.EkycModel.Liveness.ResultFace
import com.hyperlogy.smartekyc.ekyc.EkycModel.OCR.UserInfor
import com.hyperlogy.smartekyc.ekyc.R
import kotlinx.android.synthetic.main.activity_result.*
import java.io.File

private lateinit var rootOCR: UserInfor.RootOCR
//private lateinit var dataLiveness: ResultFace.DataLiveness
private lateinit var rootLiveness: ResultFace.RootLiveness
private var RESULT_CODE_TAKE_FACE_AGAIN = 3000
private lateinit var idCard: UserInfor.IdCard
class ResultActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        getUserInfor()
        getMatchFace()
        getFilePathCardFromMain()
        btn_xac_nhan_cmt.setOnClickListener(this)

    }

    private fun getMatchFace() {
        //--------------------liveness---------------------
        val intent: Intent = getIntent()
        val bundle: Bundle = intent.getExtras()!!
        if (bundle != null) {
            rootLiveness = bundle.getSerializable("rootLiveness") as ResultFace.RootLiveness
            Log.d("rootlv_result", rootLiveness.toString())
            if (rootLiveness != null) {
                if (rootLiveness.code == 200) {
                    btn_xac_nhan_cmt.setText("Đăng ký thành công")
                    txt_liveness.setText("So khớp khuôn mặt thành công")
                    txt_liveness.setTextColor(Color.parseColor("#4CAF50")) //xanh
                } else if (rootLiveness.code == 500 || rootLiveness.code == 501 || rootLiveness.code == 502) {
                    txt_liveness.setText("Lỗi hệ thống")
                    btn_xac_nhan_cmt.setText("Quay về trang chủ")
                    txt_liveness.setTextColor(Color.parseColor("#F44336")) //do
                } else {
                    txt_liveness.setText(rootLiveness.messages)
                    btn_xac_nhan_cmt.setText("Chụp lại khuôn mặt")
                    txt_liveness.setTextColor(Color.parseColor("#F44336")) //do
                }
            }
        }
    }

    private fun getFilePathCardFromMain() {
        //card
        val sharedPreferences =
            getSharedPreferences("fileCard", Context.MODE_PRIVATE)
        //face
        val sharedPreferencesFace =
            getSharedPreferences("fileImageFace", Context.MODE_PRIVATE)
        if (sharedPreferences != null) {
            val fileFrontCard = sharedPreferences.getString("fileFrontCard", "")
            val fileBackCard = sharedPreferences.getString("fileBackCard", "")
            val fileFace1 = sharedPreferencesFace.getString("nameface1", "")
            val fileFace2 = sharedPreferencesFace.getString("nameface2", "")
            val fileFace3 = sharedPreferencesFace.getString("nameface3", "")
            Log.d("fileFrontCard", fileFrontCard)
            Log.d("fileBackCard", fileBackCard)
            val fileFront = File(fileFrontCard)
            val fileBack = File(fileBackCard)
            val fileFace11 = File(fileFace1)
            val fileface22 = File(fileFace2)
            val fileFace33 = File(fileFace3)
            if (fileFront.exists() && fileBack.exists()) {
                Glide.with(applicationContext).load(fileFront).into(img_card_front)
                Glide.with(applicationContext).load(fileBack).into(img_card_back)
                Glide.with(applicationContext).load(fileFace11).into(img_face)
            } else {
                Toast.makeText(applicationContext, "File not Exist", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun getUserInfor() {
        val intent: Intent = getIntent()
        val bundle: Bundle = intent.getExtras()!!
        if (bundle != null) {
            //-----------ocr-----------------
            rootOCR = (bundle.getSerializable("rootOCR") as UserInfor.RootOCR?)!!
           // val dataOCR: UserInfor.DataOCR = rootOCR!!.data
          //  val idCard: UserInfor.IdCard = dataOCR.idCard
            idCard= bundle.getSerializable("idCard") as UserInfor.IdCard
            Log.d("idCard_result", idCard.toString())
            txt_card_type.setText(idCard.identCardType)
            txt_id_card.setText(idCard.identCardNumber)
            txt_fullname.setText(idCard.identCardName)
            txt_date_of_birth.setText(idCard.identCardBirthDate)
            txt_hometown.setText(idCard.identCardCountry)
            txt_address_residence.setText(idCard.identCardAdrResidence)
            txt_gender.setText(idCard.identCardGender)
            txt_ident_card_issue_date.setText(idCard.identCardIssueDate)
            txt_expire_date.setText(idCard.identCardExpireDate)
            if (idCard.identCardNation != "") {
                txt_nation_ethnic.setText(idCard.identCardNation)
            } else if (idCard.identCardEthnic != "") {
                txt_nation_ethnic.setText(idCard.identCardEthnic)
            }

        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btn_xac_nhan_cmt -> {
                if (rootLiveness.code == 200 || rootLiveness.code == 500 || rootLiveness.code == 501 || rootLiveness.code == 502) {
                    val intent: Intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                } else {
                        val returnIntent: Intent = Intent()
                        val bundle: Bundle= Bundle()
                        bundle.putSerializable("result_user", idCard )
                        returnIntent.putExtras(bundle)
                        setResult(RESULT_CODE_TAKE_FACE_AGAIN, returnIntent)
                        Log.d("result_user", idCard.toString())
                        finish()
                }

            }
        }
    }
}

