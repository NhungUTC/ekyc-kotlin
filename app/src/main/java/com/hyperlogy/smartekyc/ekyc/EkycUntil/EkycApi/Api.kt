package com.hyperlogy.smartekyc.ekyc.EkycUntil.EkycApi

import android.util.Log
import com.hyperlogy.smartekyc.ekyc.EkycModel.ItemFormData
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

class Api {
    companion object{
        // val PUBLIC_KEY = "6789!@%23$";
        val BASE_JWT="http://14.224.132.206:8682"
        val BASE_API="http://14.224.132.206:8681"
        val PUBLIC_KEY="Hyper@1234"
         fun getUrlGetToken(): String? {
            return BASE_JWT+"/api/token/Get?publicKey="+ PUBLIC_KEY
        }

        fun getUrlOCR(): String? {
            return BASE_API+"/api-v2/process-automation/OCR"
            //  return "http://14.224.132.206:8681/api/ProcessAutomation/OCR"
        }

        fun getUrlVerifyFace(): String? {
            return BASE_API+"/api/FaceVerify/Liveness"
        }

        fun getRequestBodyOCR(objectList: MutableList<Any>): RequestBody? {
            var MEDIA_TYPE="image/jpeg".toMediaType()
            //ảnh mặt trước, mặt sau cmt,
            Log.d("dataApiDevOCR", objectList.toString())
            val imageFront: ItemFormData = objectList[0] as ItemFormData
            val imageBack: ItemFormData = objectList[1] as ItemFormData
            return MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(
                    "IdCardFront",
                    imageFront.Name,
                    File(imageFront.File.toString()).asRequestBody(MEDIA_TYPE)
                )
                .addFormDataPart(
                    "IdCardBack",
                    imageBack.Name,
                    File(imageBack.File.toString()).asRequestBody(MEDIA_TYPE)
                )
                .build()
        }

        fun getRequestBodyVerify(objectList: MutableList<Any>): RequestBody? {
            var MEDIA_TYPE="image/jpeg".toMediaType()
            Log.d("dataApiDevVerify", objectList.toString())
            val imgFace1: ItemFormData = objectList[0] as ItemFormData
            val imgFace2: ItemFormData = objectList[1] as ItemFormData
            val imgFace3: ItemFormData = objectList[2] as ItemFormData
            val guiId: String =objectList[3] as String
            return MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(
                    "CaptureImage1",
                    imgFace1.Name,
                    File(imgFace1.File.toString()).asRequestBody(MEDIA_TYPE)
                )
                .addFormDataPart(
                    "CaptureImage2",
                    imgFace2.Name,
                    File(imgFace2.File.toString()).asRequestBody(MEDIA_TYPE)
                )
                .addFormDataPart(
                    "CaptureImage2",
                    imgFace2.Name,
                    File(imgFace2.File.toString()).asRequestBody(MEDIA_TYPE)
                )
                .addFormDataPart(
                    "CaptureImage3",
                    imgFace3.Name,
                    File(imgFace3.File.toString()).asRequestBody(MEDIA_TYPE)
                )
                .addFormDataPart("GuidID", guiId)
                .build()
        }
    }

}