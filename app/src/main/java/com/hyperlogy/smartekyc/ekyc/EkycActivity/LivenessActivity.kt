package com.hyperlogy.smartekyc.ekyc.EkycActivity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.util.Size
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import com.google.common.util.concurrent.ListenableFuture
import com.google.gson.Gson
import com.hyperlogy.smartekyc.ekyc.EkycAction.ApiHelper
import com.hyperlogy.smartekyc.ekyc.EkycAction.GetData
import com.hyperlogy.smartekyc.ekyc.EkycAction.EkycMyCall.OnPostDataSuccess
import com.hyperlogy.smartekyc.ekyc.EkycModel.ItemFormData
import com.hyperlogy.smartekyc.ekyc.EkycModel.Liveness.ResultFace
import com.hyperlogy.smartekyc.ekyc.EkycModel.OCR.UserInfor
import com.hyperlogy.smartekyc.ekyc.EkycUntil.EkycApi.Api
import com.hyperlogy.smartekyc.ekyc.databinding.ActivityLivenessBinding
import kotlinx.android.synthetic.main.activity_liveness.*
import okhttp3.RequestBody
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors

private const val REQUEST_CODE_PERMISSIONS = 10

private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)

class LivenessActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLivenessBinding

    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>

    private lateinit var imagePreview: Preview

    private lateinit var imageAnalysis: ImageAnalysis

    private lateinit var imageCapture: ImageCapture

    private lateinit var previewView: PreviewView

    private val executor = Executors.newSingleThreadExecutor()

    private lateinit var outputDirectory: File

    private lateinit var cameraControl: CameraControl

    private lateinit var cameraInfo: CameraInfo
    private var lensFacing: Int = CameraSelector.LENS_FACING_FRONT

    private var listImageFace: ArrayList<ItemFormData> =
        arrayListOf<ItemFormData>()// luu anh chup khuon mat
    private var COUNT = 0;
    private lateinit var _rootOCR: UserInfor.RootOCR
    private lateinit var _userInfor: UserInfor.IdCard
    private lateinit var _userInforSend: UserInfor.IdCard
    private lateinit var _userInforFromResult: UserInfor.IdCard
    private var i = 0// bien set progress take face
    private var REQUEST_CODE_START_RESULT_ACTIVITY = 100
    private var RESULT_CODE_TAKE_FACE_AGAIN = 3000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLivenessBinding.inflate(layoutInflater)
        setContentView(binding.root)
        previewView = binding.previewViewLiveness
        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        if (allPermissionsGranted()) {
            previewView.post { startCamera(lensFacing) }
        } else {
            requestPermissions(
                REQUIRED_PERMISSIONS,
                REQUEST_CODE_PERMISSIONS
            )
        }
        changeFrontCamera()
        getUserInforFromOcr()
    }

    private fun getUserInforFromOcr() {
        val intent: Intent = getIntent()
        val bundle: Bundle = intent.getExtras()!!
        if (bundle != null) {
            //-----------ocr-----------------
            _rootOCR = (bundle.getSerializable("rootOCR") as UserInfor.RootOCR?)!!
            val dataOCR: UserInfor.DataOCR = _rootOCR!!.data
            // val idCard: UserInfor.IdCard = dataOCR.idCard
            _userInfor = dataOCR.idCard
            Log.d("_rootOCR_liveness", _rootOCR.toString())
            Log.d("_rootOCR_liveness", dataOCR.toString())
            Log.d("_rootOCR_liveness", _userInfor.toString())
        }
    }

    private fun startCamera(mylenFacing: Int) {
        imagePreview = Preview.Builder().apply {
            setTargetAspectRatio(AspectRatio.RATIO_16_9)
            setTargetRotation(previewView.display.rotation)
        }.build()

        imageAnalysis = ImageAnalysis.Builder().apply {
            setImageQueueDepth(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
        }.build()
        //  imageAnalysis.setAnalyzer(executor, LuminosityAnalyzer())
        val builder =
            ImageCapture.Builder().setTargetResolution(Size(480, 720))
        imageCapture = builder.apply {
            setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
            setFlashMode(ImageCapture.FLASH_MODE_AUTO)
        }.build()
        val cameraSelector =
            CameraSelector.Builder().requireLensFacing(mylenFacing).build()
        //  CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_FRONT).build()
        cameraProviderFuture.addListener(Runnable {
            val cameraProvider = cameraProviderFuture.get()
            val camera = cameraProvider.bindToLifecycle(
                this@LivenessActivity,
                cameraSelector,
                imagePreview,
                // imageAnalysis,
                imageCapture
                //   videoCapture
            )
            previewView.preferredImplementationMode = PreviewView.ImplementationMode.TEXTURE_VIEW
            imagePreview.setSurfaceProvider(previewView.createSurfaceProvider(camera.cameraInfo))

            cameraControl = camera.cameraControl
            cameraInfo = camera.cameraInfo
            // setTorchStateObserver()
            //   setZoomStateObserver()
        }, ContextCompat.getMainExecutor(this@LivenessActivity))
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                previewView.post { startCamera(lensFacing) }
            } else {
                finish()
            }
        }
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun changeFrontCamera() {
        pBar_liveness.visibility = View.VISIBLE
        txt_info_pbar_livess.setText("")
        txt_info_pbar_livess.visibility = View.VISIBLE
        pBar_liveness.setProgress(10)
        i = pBar_liveness.progress
        waitTakeFace()
    }

    private fun waitTakeFace() {
        val handler = Handler()
        handler.postDelayed({
            Log.d("takeFace", "take face")
            takeFace()
        }, 2000)
    }

    private fun takeFace() {
        i += 30
        pBar_liveness.setProgress(i)
        txt_info_pbar_livess.setText(i.toString() + "/" + pBar_liveness.max)
        val mDateFormat =
            SimpleDateFormat("yyyyMMddHHmmss", Locale.US)
        val file = File(
            getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            mDateFormat.format(Date()) + ".jpg"
        )
        val outputFileOptions = ImageCapture.OutputFileOptions.Builder(file).build()
        imageCapture.takePicture(
            outputFileOptions,
            executor,
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                    Handler(Looper.getMainLooper()).post() {
                        val nameFront: String = "imageFace" + Date().time + ".jpg"
                        val type: String = "image/jpeg"
                        val itemImageFace: ItemFormData = ItemFormData(file, nameFront, type)
                        listImageFace.add(itemImageFace)
                        Log.d("item_image_face", itemImageFace.toString())
                        COUNT++
                        if (COUNT < 3) {
                            waitTakeFace()
                        } else {
                            txt_status_liveness.setText("")
                            txt_info_pbar_livess.setText("Đang xác thực khuôn mặt...")
                            Log.d("listImageFace:", listImageFace.toString())
                            // TODO doi api
                            GetData.saveImageFace(this@LivenessActivity, listImageFace)
                            val objListLiveness: MutableList<Any> =
                                GetData.saveObjectListLiveness(
                                    this@LivenessActivity,
                                    listImageFace
                                )
//                            val requestBody: RequestBody =
//                                AppConfig.apiType.getRequestBodyVerify(objListLiveness)!!
//                            val urlLiveness: String =
//                                AppConfig.apiType.getUrlVerifyFace().toString()
                            val requestBody: RequestBody = Api.getRequestBodyVerify(objListLiveness)!!
                            val urlLiveness: String =Api.getUrlVerifyFace().toString()
                            //-------------------------test chua goi api

                            ApiHelper.OkHttpPost(
                                this@LivenessActivity,
                                urlLiveness,
                                requestBody,
                                true,
                                object : OnPostDataSuccess {
                                    override fun onPostSuccess(response: String) {
                                        super.onPostSuccess(response)
                                        Handler(Looper.getMainLooper()).post() {
                                            val gson = Gson()
                                            val rootLiveness: ResultFace.RootLiveness =
                                                gson.fromJson(
                                                    response,
                                                    ResultFace.RootLiveness::class.java
                                                )
                                            Log.d("rootLiveness___", rootLiveness.toString())
                                            Log.d(
                                                "dataLivenessmain",
                                                rootLiveness.toString()
                                            )
                                            if (_userInfor != null) {
                                                _userInforSend = _userInfor
                                            } else {
                                                _userInforSend = _userInforFromResult
                                            }
                                            Log.d(
                                                "_userInforSend",
                                                _userInforSend.toString()
                                            )
                                            val intent: Intent = Intent(
                                                applicationContext,
                                                ResultActivity::class.java
                                            )
                                            val bundle: Bundle = Bundle()
                                            bundle.putSerializable("rootOCR", _rootOCR)
                                            bundle.putSerializable("idCard", _userInforSend)
                                            bundle.putSerializable(
                                                "rootLiveness",
                                                rootLiveness
                                            )
                                            intent.putExtras(bundle)
                                            startActivityForResult(
                                                intent,
                                                REQUEST_CODE_START_RESULT_ACTIVITY
                                            )
                                        }
                                    }
                                })
                            //finish  //set count =0 va xoa rong list anh chup khuon mat
                            COUNT = 0
                            listImageFace.clear()
                            pBar_liveness.visibility = View.GONE
                            pBar_liveness.setProgress(10)
                        }
                    }

                }

                override fun onError(exception: ImageCaptureException) {
                    val msg = "Photo capture failed: ${exception.message}"
                    Log.d("error_take_face", msg)
                    previewView.post {
                        Toast.makeText(this@LivenessActivity, msg, Toast.LENGTH_LONG).show()
                    }
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_START_RESULT_ACTIVITY) {
            if (resultCode == RESULT_CODE_TAKE_FACE_AGAIN) {
                _userInforFromResult = data!!.getSerializableExtra("result_user") as UserInfor.IdCard
                Log.d("userinfor4", _userInforFromResult.toString())
                txt_status_liveness.setText("Đặt mặt đối diện với camera")
                Log.d("runningtest", "onActivityResult")
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("running","onRestart" )
        changeFrontCamera()
    }

    override fun onResume() {
        super.onResume()
        Log.d("running","onResume" )
    }

    override fun onPause() {
        super.onPause()
        Log.d("running","onPause" )
    }

    override fun onStop() {
        super.onStop()
        Log.d("running","onStop" )
    }
}