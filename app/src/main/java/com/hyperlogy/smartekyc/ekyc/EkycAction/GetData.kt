package com.hyperlogy.smartekyc.ekyc.EkycAction

import android.content.Context
import android.util.Log
import com.hyperlogy.smartekyc.ekyc.EkycModel.ItemFormData

class GetData {
    companion object{
        fun saveToken(context: Context, token:String){
            val sharedPreference =  context.getSharedPreferences("myToken", Context.MODE_PRIVATE)
            var editor = sharedPreference.edit()
            editor.putString("token",token)
            editor.commit()
        }
        fun getToken(context: Context): String{
            var token:String=""
            val sharedPreference = context.getSharedPreferences("myToken", Context.MODE_PRIVATE)
            if (sharedPreference != null) {
                token = sharedPreference.getString("token", "").toString()
                Log.d("token_register", token + "")
            }
            return token
        }
        fun saveGuidId(context: Context, guidId:String){
            val sharedPreference =
                context.getSharedPreferences("myGuidId", Context.MODE_PRIVATE)
            var editor = sharedPreference.edit()
            editor.putString("guidId", guidId)
            editor.commit()
        }
      fun getGuidId(context: Context):String{
          var guiId=""
          val sharedPreference = context.getSharedPreferences("myGuidId", Context.MODE_PRIVATE)
          if (sharedPreference != null) {
              guiId = sharedPreference.getString("guidId", "").toString()
              Log.d("guiId_postface",guiId)
          }
          return guiId
      }
        fun saveObjectListOCR(listCard: ArrayList<ItemFormData>): MutableList<Any>{
            val objectList = mutableListOf<Any>()//2 anh cmt
            objectList.add(0, listCard.get(0))
            objectList.add(1, listCard.get(1))
            return objectList
        }
        fun saveObjectListLiveness(context: Context,listFace: ArrayList<ItemFormData>): MutableList<Any>{
            val guidId: String= getGuidId(context)
            val objectList = mutableListOf<Any>()//3 anh face, guiId
            objectList.add(0, listFace.get(0))
            objectList.add(1, listFace.get(1))
            objectList.add(2, listFace.get(2))
            objectList.add(3,guidId)
            return objectList
        }
        fun saveImageFace(context: Context, listFace: ArrayList<ItemFormData>){
            val sharedPreferencesFace =
                context.getSharedPreferences("fileImageFace", Context.MODE_PRIVATE)
            val editor = sharedPreferencesFace.edit()
            editor.putString("nameface1", listFace.get(0).File.toString())
            editor.putString("nameface2", listFace.get(1).File.toString())
            editor.putString("nameface3", listFace.get(2).File.toString())
            editor.commit()
        }
    }

}