package com.hyperlogy.smartekyc.ekyc.EkycModel

import java.io.File

class ItemFormData {
    private lateinit var mFile: File
    private lateinit var mName: String
    private lateinit var mType: String
    constructor()
    constructor(mFile: File, mName: String, mType: String) {
        this.mFile = mFile
        this.mName = mName
        this.mType = mType
    }
    public var  File: File
    get(){
        return mFile
    }
    set(_file){
        this.mFile=_file
    }
    public var Name: String
    get(){
        return mName
    }
    set(_name){
        this.mName= _name
    }
    public var Type: String
    get(){
        return mType
    }
    set(_type){
         this.mType= _type
    }

    override fun toString(): String {
        return "ItemFormData(mFile=$mFile, mName='$mName', mType='$mType')"
    }

}