package com.hyperlogy.smartekyc.ekyc.EkycActivity

//import com.hyperlogy.smartekyc.databinding.MainActivityBinding
import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.util.Size
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.common.util.concurrent.ListenableFuture
import com.google.gson.Gson
import com.hyperlogy.smartekyc.ekyc.EkycAction.ApiHelper
import com.hyperlogy.smartekyc.ekyc.EkycAction.GetData
import com.hyperlogy.smartekyc.ekyc.EkycAction.MessageHelper
import com.hyperlogy.smartekyc.ekyc.EkycAction.EkycMyCall.OnMyBtnDialog
import com.hyperlogy.smartekyc.ekyc.EkycAction.EkycMyCall.OnPostDataSuccess
import com.hyperlogy.smartekyc.ekyc.EkycModel.ItemFormData
import com.hyperlogy.smartekyc.ekyc.EkycModel.OCR.UserInfor
import com.hyperlogy.smartekyc.ekyc.R
import com.hyperlogy.smartekyc.ekyc.EkycUntil.EkycApi.Api
import com.hyperlogy.smartekyc.ekyc.databinding.OcrActivityBinding
import kotlinx.android.synthetic.main.ocr_activity.*
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors


private const val REQUEST_CODE_PERMISSIONS = 10

private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)

@SuppressLint("RestrictedApi")
class OcrActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: OcrActivityBinding

    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>

    private lateinit var imagePreview: Preview

    private lateinit var imageAnalysis: ImageAnalysis

    private lateinit var imageCapture: ImageCapture

    private lateinit var previewView: PreviewView

    private val executor = Executors.newSingleThreadExecutor()

    private lateinit var outputDirectory: File

    private lateinit var cameraControl: CameraControl

    private lateinit var cameraInfo: CameraInfo
    private var lensFacing: Int = CameraSelector.LENS_FACING_BACK

    private var front = false; //check dang chup mat truoc cmt
    private lateinit var itemCardFront: ItemFormData // lưu thông tin card front
    private lateinit var itemCardBack: ItemFormData // lưu thông tin card back
    private lateinit var token: String

    private var listImageFace: ArrayList<ItemFormData> =
        arrayListOf<ItemFormData>()// luu anh chup khuon mat
    private var COUNT = 0;
    private var successOCR = false
    private lateinit var _rootOCR: UserInfor.RootOCR

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OcrActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        previewView = binding.previewView
        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        setVisibleButton()
        if (allPermissionsGranted()) {
            previewView.post { startCamera(lensFacing) }
        } else {
            requestPermissions(
                REQUIRED_PERMISSIONS,
                REQUEST_CODE_PERMISSIONS
            )
        }
        buttonSetOnCLick()
    }

    private fun setVisibleButton() {
        btn_ok_1.visibility = View.GONE
        btn_take_front_card.visibility = View.VISIBLE
        btn_Lam_lai_1.visibility = View.GONE
        txt_status_take_picture.setText("Chụp mặt trước")
    }

    private fun buttonSetOnCLick() {
        binding.btnTakeFrontCard.setOnClickListener(this)
        binding.btnOk1.setOnClickListener(this)
        binding.btnLamLai1.setOnClickListener(this)
        binding.btnTakeBackCard.setOnClickListener(this)
        binding.btnOk2.setOnClickListener(this)
        binding.btnLamLai2.setOnClickListener(this)
        binding.btnTakeFace.setOnClickListener(this)
        binding.btnSuccessOcr.setOnClickListener(this)
    }

    private fun toggleTorch() {
        if (cameraInfo.torchState.value == TorchState.ON) {
            cameraControl.enableTorch(false)
        } else {
            cameraControl.enableTorch(true)
        }
    }

    private fun takePicture() {
        if (front == true) {
            btn_take_front_card.setText("Đang chụp...");
            btn_Lam_lai_1.visibility = View.GONE
            btn_ok_1.visibility = View.GONE
        } else {
            btn_take_back_card.setText("Đang chụp...");
            btn_Lam_lai_2.visibility = View.GONE
            btn_ok_2.visibility = View.GONE
        }
        val mDateFormat =
            SimpleDateFormat("yyyyMMddHHmmss", Locale.US)
        val file = File(
            getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            mDateFormat.format(Date()) + ".jpg"
        )
        val outputFileOptions = ImageCapture.OutputFileOptions.Builder(file).build()
        imageCapture.takePicture(
            outputFileOptions,
            executor,
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                    Handler(Looper.getMainLooper()).post {
                        val msg = "Photo capture succeeded: ${file.absolutePath}"
                        //sharedPreferences lưu path file card front, card back
                        val sharedPreferences =
                            getSharedPreferences("fileCard", Context.MODE_PRIVATE)
                        val editor = sharedPreferences.edit()
                        //----------------------------------------------------------------
                        Log.d("front_image", front.toString())
                        if (front == true) {
                            //  btn_take_front_card.visibility = View.GONE
                            btn_take_front_card.visibility = View.GONE
                            btn_take_front_card.setText("Chụp mặt trước")
                            btn_Lam_lai_1.visibility = View.VISIBLE
                            btn_ok_1.visibility = View.VISIBLE
                            img_take_card.visibility = View.VISIBLE
                            val fileFront = file
                            imageTakeCard(fileFront)
                            //sharedPreferences
                            editor.putString("fileFrontCard", fileFront.toString())//
                            val nameFront: String = "fontIdCard" + Date().time + ".jpg"
                            editor.putString("nameFrontCard", nameFront)
                            val type: String = "image/jpeg"
                            itemCardFront = ItemFormData(fileFront, nameFront, type)
                            Log.d("item_card_front:", itemCardFront.toString())
                        } else {
                            btn_take_back_card.visibility = View.GONE
                            btn_take_back_card.setText("Chụp mặt sau")
                            btn_Lam_lai_2.visibility = View.VISIBLE
                            btn_ok_2.visibility = View.VISIBLE
                            img_take_card.visibility = View.VISIBLE
                            val fileBack = file
                            imageTakeCard(fileBack)
                            //sharedPreferences
                            editor.putString("fileBackCard", fileBack.toString())//
                            val nameBack: String = "backIdCard" + Date().time + ".jpg"
                            val type: String = "image/jpeg"
                            itemCardBack = ItemFormData(fileBack, nameBack, type)
                            Log.d("item_card_front:", itemCardBack.toString())
                        }
                        //sharedPreferences
                        editor.commit()
                        previewView.post {
                            // Toast.makeText(this@MainActivity, msg, Toast.LENGTH_LONG).show()
                            // Toast.makeText(this@MainActivity, "Đang chụp...", Toast.LENGTH_LONG).show()
                        }
                    }
                }

                override fun onError(exception: ImageCaptureException) {
                    val msg = "Photo capture failed: ${exception.message}"
                    previewView.post {
                        Toast.makeText(this@OcrActivity, msg, Toast.LENGTH_LONG).show()
                    }
                }
            })
    }

    private fun imageTakeCard(mfileCard: File) {
        if (mfileCard.exists()) {
            Glide.with(applicationContext).load(mfileCard).into(img_take_card)
        } else {
            Toast.makeText(applicationContext, "File not exists", Toast.LENGTH_LONG).show()
        }
    }

    private fun startCamera(mylenFacing: Int) {
        imagePreview = Preview.Builder().apply {
            setTargetAspectRatio(AspectRatio.RATIO_16_9)
            setTargetRotation(previewView.display.rotation)
        }.build()

        imageAnalysis = ImageAnalysis.Builder().apply {
            setImageQueueDepth(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
        }.build()
        //  imageAnalysis.setAnalyzer(executor, LuminosityAnalyzer())

        imageCapture = ImageCapture.Builder().apply {
            setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
            setFlashMode(ImageCapture.FLASH_MODE_AUTO)
            setTargetResolution(Size(480, 720))
        }.build()
        val cameraSelector =
            CameraSelector.Builder().requireLensFacing(mylenFacing).build()
        //  CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_FRONT).build()
        cameraProviderFuture.addListener(Runnable {
            val cameraProvider = cameraProviderFuture.get()
            val camera = cameraProvider.bindToLifecycle(
                this@OcrActivity,
                cameraSelector,
                imagePreview,
                // imageAnalysis,
                imageCapture
                //   videoCapture
            )
            previewView.preferredImplementationMode = PreviewView.ImplementationMode.TEXTURE_VIEW
            imagePreview.setSurfaceProvider(previewView.createSurfaceProvider(camera.cameraInfo))

            cameraControl = camera.cameraControl
            cameraInfo = camera.cameraInfo
            // setTorchStateObserver()
            //   setZoomStateObserver()
        }, ContextCompat.getMainExecutor(this@OcrActivity))
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                previewView.post { startCamera(lensFacing) }
            } else {
                finish()
            }
        }
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            //take front card
            R.id.btn_take_front_card -> {
                front = true
                takePicture()
            }
            R.id.btn_Lam_lai_1 -> {
                btn_take_front_card.visibility = View.VISIBLE
                btn_Lam_lai_1.visibility = (View.GONE)
                btn_ok_1.visibility = (View.GONE)
                img_take_card.visibility = View.GONE
            }
            R.id.btn_ok_1 -> {
                //  view_take_front_card.visibility = View.GONE
                view_take_back_card.visibility = View.VISIBLE
                btn_Lam_lai_1.visibility = View.GONE
                btn_ok_1.visibility = View.GONE
                btn_take_front_card.visibility = View.GONE

                btn_take_back_card.visibility = View.VISIBLE
                btn_ok_2.visibility = View.GONE
                btn_Lam_lai_2.visibility = View.GONE
                txt_status_take_picture.setText("Chụp mặt sau")
                img_take_card.visibility = View.GONE
            }
            //take back card
            R.id.btn_take_back_card -> {
                front = false// chup mat sau cmt
                takePicture()
            }
            R.id.btn_Lam_lai_2 -> {
                btn_take_back_card.setVisibility(View.VISIBLE)
                btn_Lam_lai_2.setVisibility(View.GONE)
                btn_ok_2.setVisibility(View.GONE)
                img_take_card.visibility = View.GONE
            }
            //gui form data
            R.id.btn_ok_2 -> {
                //TODO doi api
                img_take_card.visibility = View.VISIBLE
                btn_ok_2.visibility = View.GONE
                btn_Lam_lai_2.visibility = View.GONE
                /*changeFrontCamera()*/
                process_bar_ocr.visibility = View.VISIBLE
                txt_status_take_picture.setText("Đang kiểm tra giấy tờ...")
                createFormData()
            }
            //test success OCR
            R.id.btn_success_ocr -> {
                img_take_card.visibility = View.GONE
                if (successOCR == true) {
                    btn_success_ocr.visibility = View.GONE
                    val intent: Intent = Intent(applicationContext, LivenessActivity::class.java)
                    val bundle: Bundle = Bundle();
                    bundle.putSerializable("rootOCR", _rootOCR)
                    intent.putExtras(bundle)
                    startActivity(intent)
                    finish()
                }
//                else if (successOCR == false) {
//                    btn_success_ocr.visibility = View.GONE
//                    btn_take_front_card.visibility= View.VISIBLE
//                    setVisibleButton()
//                }
            }
            //test take face
            R.id.btn_take_face -> {
                //  takeFace()
            }
        }
    }

    private fun createFormData() {
        val listCard: ArrayList<ItemFormData> = arrayListOf()
        listCard.add(itemCardFront)
        listCard.add(itemCardBack)
        val objListOCR: MutableList<Any> = GetData.saveObjectListOCR(listCard)
//        val requestBody: RequestBody? = AppConfig.apiType.getRequestBodyOCR(objListOCR)
//        val urlOCr: String? = AppConfig.apiType.getUrlOCR()
        val requestBody: RequestBody? =Api.getRequestBodyOCR(objListOCR)
        val urlOCr: String? = Api.getUrlOCR()
        ApiHelper.OkHttpPost(
            this@OcrActivity,
            urlOCr!!,
            requestBody!!,
            true,
            object : OnPostDataSuccess {
                override fun onPostSuccess(response: String) {
                    super.onPostSuccess(response)
                    var obj: JSONObject = JSONObject(response)
                    val gson = Gson()
                    val rootOCR: UserInfor.RootOCR =
                        gson.fromJson(response, UserInfor.RootOCR::class.java)
                    Log.d("jsonData", rootOCR.toString())
                    Handler(Looper.getMainLooper()).post() {
                        _rootOCR = rootOCR
                        val _dataOCR: UserInfor.DataOCR = rootOCR.data
                        if (rootOCR.code == 200) {
                            val _idCard: UserInfor.IdCard = _dataOCR.idCard
                            var guidId: String = _dataOCR.guidID
                            GetData.saveGuidId(this@OcrActivity, guidId)
                            //luu guidId vao sharedPreference
                            //  txt_status_take_picture.setText(rootOCR.messages)//----------------setText("")
                            txt_status_take_picture.setText("Kiểm tra giấy tờ thành công")//----------------setText("")
                            btn_success_ocr.setText("Kiểm tra khuôn mặt")
                            successOCR = true;
                            process_bar_ocr.visibility = View.GONE
                            btn_success_ocr.visibility = View.VISIBLE

                        } else {
                            successOCR = false
                            txt_status_take_picture.setText("")
                            process_bar_ocr.visibility = View.GONE
//                            btn_success_ocr.visibility = View.VISIBLE
//                            btn_success_ocr.setText("Chụp lại giấy tờ")
                            btn_success_ocr.visibility=View.GONE
                            //TODO
                            val mess: String
                            if (rootOCR.code == 501 || rootOCR.code == 502 || rootOCR.code == 503) {
                                mess = MessageHelper.messCode500
                            } else {
                                mess = rootOCR.messages
                            }
                            MessageHelper.openDialog(
                                this@OcrActivity,
                                "Thông báo",
                                mess,
                                "Bỏ qua",
                                "Làm lại",
                                object :
                                    OnMyBtnDialog {
                                    override fun onCancle(isCancle: Boolean) {
                                        if (isCancle) {
                                            val intent: Intent =
                                                Intent(this@OcrActivity, LoginActivity::class.java)
                                            startActivity(intent)
                                        }
                                    }

                                    override fun onTryAgain(isTryAgain: Boolean) {
                                        if (isTryAgain) {
                                            btn_success_ocr.visibility = View.GONE
                                            btn_take_front_card.visibility = View.VISIBLE
                                            img_take_card.visibility=View.GONE
                                            setVisibleButton()
                                        }
                                    }

                                });
                            Log.d("dataOCR", "dataOCR null")
                        }
                    }
                }
            })
    }
}
