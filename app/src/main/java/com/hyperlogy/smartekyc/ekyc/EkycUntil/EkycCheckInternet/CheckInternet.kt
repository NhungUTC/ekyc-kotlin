package com.hyperlogy.smartekyc.ekyc.EkycUntil.EkycCheckInternet

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.hyperlogy.smartekyc.ekyc.EkycActivity.LoginActivity

class CheckInternet {
    companion object {
        fun haveNetworkConnection(context: Context): Boolean {
            var haveConnectedWifi = false
            var haveConnectedMobile = false
            val cm =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.allNetworkInfo
            for (ni in netInfo) {
                if (ni.typeName
                        .equals("WIFI", ignoreCase = true)
                ) if (ni.isConnected) haveConnectedWifi = true
                if (ni.typeName
                        .equals("MOBILE", ignoreCase = true)
                ) if (ni.isConnected) haveConnectedMobile = true
            }
            return haveConnectedWifi || haveConnectedMobile
        }
        fun dialogGetToken(
            context: Context?,
            title: String?,
            message: String?,
            textPBtn: String?
        ) {
            val b = AlertDialog.Builder(context)
            b.setTitle(title)
            b.setMessage(message)
            b.setPositiveButton(
                textPBtn
            ) { dialog, which -> }
            b.show()
        }

        fun checkTimeoutPost(
            context: Context,
            title: String?,
            message: String?,
            textBtn: String?
        ) {
            val b = AlertDialog.Builder(context)
            b.setTitle(title)
            b.setMessage(message)
            b.setPositiveButton(
                textBtn
            ) { dialog, which ->
                val intent = Intent(context, LoginActivity::class.java)
                context.startActivity(intent)
            }
            b.show()
        }
    }
}